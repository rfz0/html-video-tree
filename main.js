'use strict';

window.Globals = {};

(function() {

    function addEvent(obj, ev, fn, inst) {
        var cb = inst ? function(ev) {
                fn.apply(inst, [ev]);
            } : fn;

        if (obj.addEventListener) {
            obj.addEventListener(ev, cb, false);
        } else if (obj.attachEvent) {
            obj.attachEvent('on' + ev, cb);
        }
    }

    var Tree = {

        create: function(a) {
            var t = [];

            for (var i = 0, n = a.length; i < n; i++) {
                Tree.insert(t, a[i].split('/'));
            }

            return t;
        },

        insert: function(t, a) {
            for (var i = 0, n = a.length - 1; i < n; i++) {
                t = t[a[i]] = t[a[i]] || {}
            }

            t[a[i]] = null;
        },
    };

    var Header = function(id, ext) {
        this.parent = document.getElementById(id);
        this.onclick = null;
    };

    Header.prototype = {

        create: function(a) {
            var ul = document.createElement('ul');

            for (var i = 0, n = a.length; i < n; i++) {
                var anchor = document.createElement('a');
                anchor.href = 'javascript:;';
                anchor.onclick = this.onclick;

                var img = document.createElement('img');
                img.src = a[i] + '.small.' + window.Globals.config.ext;
                img.name = a[i];
                img.title = a[i];

                anchor.appendChild(img);

                var li = document.createElement('li');

                li.appendChild(anchor);

                ul.appendChild(li);
            }

            if (this.parent.firstChild) {
                this.parent.removeChild(this.parent.firstChild);
            }

            this.parent.appendChild(ul);
        }
    };

    var Sidebar = function(id) {
        this.closeSymbol = '[-]';
        this.openSymbol = '[+]';
        this.onclick = null;
        this.parent = document.getElementById(id);
        this.view = null;
    };

    Sidebar.prototype = {

        addEvent: function(id, ev, fn) {
            addEvent(document.getElementById(id), ev, fn, this);
        },

        create: function(a) {
            this.view = this.convert(Tree.create(a), null);

            this.init();

            if (this.parent.firstChild)
                this.parent.removeChild(this.parent.firstChild);

            this.parent.appendChild(this.view);
        },

        convert: function(tree, subtree) {
            var subtree = subtree || [];
            var el = document.createElement('ul');

            el.className = 'treeview';

            for (var node in tree) {
                subtree.push(node);

                var li = document.createElement('li');

                if (tree[node] != null) {
                    li.innerHTML = node;
                    li.appendChild(this.convert(tree[node], subtree));
                } else {
                    var anchor = document.createElement('a');
                    anchor.href = 'javascript:;';
                    anchor.name = subtree.join('/');
                    anchor.onclick = this.onclick;

                    anchor.appendChild(document.createTextNode(node));

                    li.appendChild(anchor);
                }

                el.appendChild(li);

                subtree.pop(node);
            }
            return el;
        },

        init: function() {
            this.view.id = 'treeview';

            var els = this.view.getElementsByTagName('ul');

            for (var i = 0, n = els.length; i < n; i++) {
                els[i].parentNode.firstChild.data = this.openSymbol + ' ' +
                    els[i].parentNode.firstChild.data;
                els[i].parentNode.className = 'subtree';
                els[i].style.display = 'none';

                addEvent(els[i].parentNode, 'click', this.toggle, this);

                els[i].onclick = function(e) {
                    if (typeof e != 'undefined') {
                        e.stopPropagation();
                    } else {
                        event.cancelBubble = true
                    }
                };
            }
        },

        toggle: function(e) {
            var el = e.target ? e.target.getElementsByTagName('ul')[0] :
                e.tagName == 'UL' ? e :
                e.srcElement.tagName == 'UL' ? e.srcElement :
                e.srcElement.getElementsByTagName('ul')[0];

            el.style.display = (el.style.display == 'block') ? 'none' : 'block';

            el.parentNode.firstChild.data =
                el.parentNode.firstChild.data.replace(
            (el.style.display == 'block') ? this.openSymbol : this.closeSymbol,
                (el.style.display == 'block') ? this.closeSymbol : this.openSymbol);
        },

        flatten: function(collapse) {
            var els = document.getElementById(this.view.id).getElementsByTagName('ul');

            for (var i = 0, n = els.length; i < n; i++) {
                if (els[i].style.display == ((collapse == true) ? 'block' : 'none')) {
                    this.toggle(els[i]);
                }
            }
        }
    };

    function Model(data) {
        this.data = data;
        this.idx = 0;
    };

    Model.prototype = {

        get: function() {
            return this.data[this.idx];
        },

        set: function(s) {
            this.idx = this.data.indexOf(s);
        },

        next: function() {
            this.idx = Math.min(this.data.length - 1, this.idx + 1);
            return this.data[this.idx];
        },

        prev: function() {
            this.idx = Math.max(0, this.idx - 1);
            return this.data[this.idx];
        },
    };

    function View(id) {
        this.view = document.getElementById(id);
    };

    View.prototype = {

        display: function(s) {
            this.view.getElementsByTagName('h3')[0].innerHTML = s;

            var anchor = this.view.getElementsByTagName('a')[0];
            anchor.href = '../' + s;
            anchor.getElementsByTagName('img')[0].src = s + '.large.' +
		    window.Globals.config.ext;

            document.getElementById('download').href = anchor.href;
        },
    };


    function setHash(s) {
        window.location.hash = '#' + s;
    }

    function getHash() {
        return decodeURI(window.location.hash.replace('#', ''));
    }

    window.onload = function() {
        (function() {
            var model = new Model(window.Globals.data);
            var view = new View('content');

            addEvent(document.getElementById('next'), 'click',
                function() {
                    setHash(model.next());
                });

            addEvent(document.getElementById('prev'), 'click',
                function() {
                    setHash(model.prev());
                });

            var header = new Header('header');

            var sidebar = new Sidebar('sidebar');

            sidebar.addEvent('collapse', 'click', function() {
                sidebar.flatten(true);
            });

            sidebar.addEvent('expand', 'click', function() {
                sidebar.flatten(false);
            });

            sidebar.onclick = header.onclick = function() {
                var s = arguments[0].target ? arguments[0].target.name :
                    arguments[0].srcElement ?
                    arguments[0].srcElement.name : arguments[0];
                setHash(s);
            };

            window.onhashchange = function() {
                var hash = getHash();
                model.set(hash);
                view.display(hash);
            };

            if (getHash() == '') {
                setHash(model.get());
            } else {
                window.onhashchange();
            }

            sidebar.create(model.data);

            header.create(model.data);
        })();
    };
})();
