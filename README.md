# HTML Video Thumbnail Gallery
Create a minimal, static webpage of a directory tree of videos

**Dependencies**  
Python, ImageMagick and MPlayer

**Usage**  

	git clone ... gallery
	cd path/to/videos  
	python path/to/gallery
	xdg-open html/index.html 

**Options**  
Options are stored in options.cfg which is created if it does not exist


**Clean**

        rm -r options.cfg html

