#!/usr/bin/env python

import os
import subprocess
import sys
import time
import shutil
import ConfigParser

from capture import Capture
from status import Status

# Directory containing this file and other scripts
PATH = os.path.dirname(os.path.realpath(__file__))

def mkdir(d):
    try:
        os.makedirs(d)
    except OSError as ex:
        if ex.errno == os.errno.EEXIST and os.path.isdir(d):
            pass
        else:
            raise

def find(ign, exts):
    cmd = 'find . \\! \\( %s \\) -type f \\( %s \\)' % (
            ' -or '.join(["-path '%s' -prune" % s.strip() for s in
                ign.split(',')]),
            ' -or '.join(["-iname '*.%s'" % s.strip() for s in
                exts.split(',')]))
    proc = subprocess.Popen(cmd,
            shell=True,
            stdin=file(os.devnull, 'r'),
            stdout=subprocess.PIPE,
            stderr=file(os.devnull, 'w'))
    return [line.replace('./', '').rstrip() for line in
            proc.stdout.readlines() if line]

def capture(src, opts):
    dst = os.sep.join([opts['main']['dst'], src])
    ext = opts['capture']['ext']
    img = '.'.join([dst, 'large', ext])
    if not os.path.exists(img):
        Capture(src, **opts['capture']).run()
        mkdir(os.path.dirname(dst))
        try:
            shutil.move('.'.join([src, ext]), img)
        except:
            return False
    tn = '.'.join([dst, 'small', ext])
    if not os.path.exists(tn):
        try:
            subprocess.check_call(['convert', '-thumbnail',
                opts['main']['geom'], img, tn], stderr=file(os.devnull,
                    'w'))
        except:
            return False
    return True

def write_js(opts, files, fname):
    try:
        with open(fname, 'w') as f:
            f.write('window.Globals.config = {};\n')
            f.write('window.Globals.config.ext = "%s";\n' %
                    opts['capture']['ext'])

            f.write('window.Globals.data = [];\n')
            for s in files:
                f.write('window.Globals.data.push("%s");\n' % s)
    except IOError as ex:
        sys.stderr.write(ex)
        sys.exit(1)

def parse_option(s):
    if s.lower() in ('yes', 'true'):
        return True
    if s.lower() in ('no', 'false'):
        return False
    try:
        s = int(s)
    except:
        pass
    return s

def read_config(fname):
    cfg = ConfigParser.ConfigParser()
    cfg.read(fname)
    opts = {}
    for sect in cfg.sections():
        opts[sect] = {}
        for opt in cfg.options(sect):
            try:
                opts[sect][opt] = parse_option(cfg.get(sect, opt))
            except:
                opts[sect][opt] = None
    return opts

def copy_files(dst, *src):
    for f in src:
        if not os.path.exists(os.sep.join([dst, f])):
            shutil.copy(os.sep.join([PATH, f]), dst)

def main():
    copy_files(os.getcwd(), 'options.cfg')
    opts = read_config('options.cfg')
    dst = opts['main']['dst']
    mkdir(dst)
    copy_files(dst, 'index.html', 'default.css', 'main.js')
    files = find(opts['main']['ign'], opts['main']['exts'])
    failed = []
    status = Status(total=len(files))
    i = 1
    verbose = opts['main']['verbose']
    for f in files:
        if verbose:
            status.display(i, f)
            sys.stdout.write('\n')
            sys.stdout.flush()
        if capture(f, opts) == False:
            failed.append(f)
        if verbose:
            status.clear()
        i += 1
    files = [f for f in files if not f in failed]
    write_js(opts, files, os.sep.join([dst, 'data.js']))
    if verbose and len(failed) > 0:
        print >> sys.stderr, 'Failed:'
        for f in failed:
            print >> sys.stderr, f

if __name__ == '__main__':
    main()
