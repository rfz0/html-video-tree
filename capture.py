import os
import signal
import subprocess
import sys
from Queue import Queue
from datetime import datetime
from shutil import rmtree
from tempfile import mkdtemp
from threading import Thread, Event
from time import sleep

from status import Status

class Capture:

    MPLAYER_COMMAND = ['mplayer', '-really-quiet', '-noconfig', 'all',
            '-nolirc', '-ao', 'null', '-nosound', '-frames', '1']

    MPLAYER_FRAMEOUT = '00000001'

    def __init__(self, fname, **kwargs):
        self.fname = fname
        vars(self).update(kwargs)
        self.queue = Queue()
        self.event = Event()
        self.outdir = None
        self.ndone = None
        self.props = None
        signal.signal(signal.SIGTERM, self.sig_handler)
        signal.signal(signal.SIGINT, self.sig_handler)

    def clean(self):
        rmtree(self.outdir, ignore_errors=True)

    def sig_handler(self, signum, stk):
        self.event.set()
        if self.verbose:
            sys.stderr.write(os.linesep + 'Caught signal %d' % signum)
        self.clean()
        sys.exit(1 + signum)

    def identify(self, fname):
        proc = subprocess.Popen(self.MPLAYER_COMMAND + [ '-vo', 'null',
            '-msglevel', 'identify=4', fname],
            stdin=file(os.devnull, 'r'),
            stdout=subprocess.PIPE,
            stderr=file(os.devnull, 'w'))
        return [line for line in proc.stdout.readlines() if line]

    def property(self, *args):
        if not self.props or self.props.get('ID_FILENAME', None) != \
            self.fname:
            self.props = dict([prop.split('=') for prop in
                self.identify(self.fname)])
        return (self.props[key] for key in args if key in self.props)

    def len(self):
        try:
            length, = self.property('ID_LENGTH')
        except ValueError:
            return 0
        return float(length)

    def capture_frame(self, startsec, fname):
        try:
            subdir = mkdtemp(dir=self.outdir)
            geom = ':'.join(self.geom.split('x'))
            subprocess.call(self.MPLAYER_COMMAND + ['-ss', str(startsec),
                    '-vf', 'scale=' + geom,
                    '-vo', 'jpeg:outdir=' + self.outdir
                    + ':subdirs=' + os.path.basename(subdir),
                    self.fname],
                    stdin=file(os.devnull, 'r'),
                    stdout=file(os.devnull, 'w'),
                    stderr=subprocess.STDOUT)
            img = os.sep.join([self.outdir, os.path.basename(subdir) +
                self.MPLAYER_FRAMEOUT, self.MPLAYER_FRAMEOUT + '.jpg'])
            if os.path.exists(img):
                os.rename(img, os.sep.join([self.outdir, fname]))
            os.rmdir(subdir)
            rmtree(subdir + self.MPLAYER_FRAMEOUT)
        except:
            pass

    def montage(self):
        try:
            cmd = ['montage', '-geometry', self.geom] + [self.outdir +
                    os.sep + '%d.jpg' % i for i in range(self.nframes +
                        1)]
            cmd.append(self.fname + '.' + self.ext)
            subprocess.call(cmd,
                    stdin=file(os.devnull, 'r'),
                    stdout=file(os.devnull, 'w'),
                    stderr=subprocess.STDOUT)
        except OSError as ex:
            sys.exit(ex)

    def run(self):
        def timer():
            status = Status(total=self.nframes)
            while not self.event.is_set():
                status.display(self.ndone, self.fname)
                sleep(0.1)

        def target():
            length = self.len()
            while not self.event.is_set():
                framenum = self.queue.get()
                if length > 0:
                    self.capture_frame(framenum * length /
                self.nframes, '%i.jpg' % framenum)
                    self.ndone += 1
                self.queue.task_done()

        self.ndone = 0
        self.outdir = mkdtemp()
        if self.verbose:
            thread = Thread(target=timer)
            thread.daemon = True
            thread.start()
        for i in range(self.jobs):
            thread = Thread(target=target)
            thread.daemon = True
            thread.start()
        for framenum in range(1, self.nframes + 1):
            self.queue.put(framenum)
        try:
            while self.queue.unfinished_tasks:
                sleep(1)
        except (KeyboardInterrupt, SystemExit):
            self.event.set()
        self.event.set()
        self.montage()
        self.clean()
