import os
import sys
from math import log10
from datetime import datetime

class Status:
    def __init__(self, **kwargs):
        self.start = datetime.now().replace(microsecond=0)
        self.total = kwargs.get('total', 0)
        if self.total > 0:
            self.pad = int(log10(self.total)) + 1
        else:
            self.pad = 0
        self.width = os.getenv('COLUMNS', 80)

    def display(self, index, desc):
        now = datetime.now().replace(microsecond=0)
        output = '\r\033[K%s %0*d/%0*d %s' % (now - self.start,
                self.pad, index,
                self.pad, self.total,
                desc)
        if len(output) > self.width:
            output = output[:self.width-3] + '...'
        sys.stdout.write(output)
        sys.stdout.flush()

    def clear(self):
        sys.stdout.write('\r\033[K\033[1A\033[K')
        sys.stdout.flush()

